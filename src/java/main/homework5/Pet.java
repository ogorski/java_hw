package homework5;

import java.util.*;

public class Pet {
    private String nickname,
            species;
    private String[] habits;

    public void setNickname(String nickname) {this.nickname = nickname;}
    public String getNickname() {return nickname;}
    public void setSpecies(String species) {this.species = species;}
    public String getSpecies() {return species;}
    public void setHabits(String[] habits) {this.habits = habits;}
    public String[] getHabits() {return habits;}

    private int age,
            trickLvl;
    private ConstructorTypes currentType;
    public void setAge(int age) {this.age = age;}
    public int getAge() {return age;}
    public void setTrickLvl(int trickLvl) {this.trickLvl = trickLvl;}
    public int getTrickLvl() {return trickLvl;}

    private enum ConstructorTypes {
        SMALL("%s{nickname='%s'}"),
        MAXIMUM("%s{nickname='%s, age=%d, trickLvl=%d, habits=%s}"),
        EMPTY("pet{}");
        String toStringText;
        ConstructorTypes(String toStringText) {
            this.toStringText = toStringText;
        }
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.currentType = ConstructorTypes.SMALL;
    }

    public Pet(String species, String nickname, int age, int trickLvl, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLvl = trickLvl;
        this.habits = Arrays.copyOf(habits, habits.length);
        this.currentType = ConstructorTypes.MAXIMUM;
    }

    public Pet() {
        this.currentType = ConstructorTypes.EMPTY;
    }

    public static void eat() {
        System.out.println("I`m eating!");
    }

    public void respond() {
        System.out.printf("\n Hello, friend! I was missing you!", nickname);
    }

    public static void foul() {
        System.out.println("I need to get rid of it....");
    }

    @Override
    public String toString() {
        return String.format(currentType.toStringText, nickname, species, age, trickLvl, Arrays.toString(habits));
    }

    {
        System.out.println("Creating object Pet");
    }
    {
        System.out.println("Downloading class Pet");
    }

    protected void finalize() {
        System.out.println("Deleting object: " + this);
    }

}
