package homework5;

import java.util.*;

public enum Week {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public String getValue() {return this.name().toLowerCase(Locale.ROOT);}
}
