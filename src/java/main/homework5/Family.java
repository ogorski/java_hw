package homework5;

import java.util.*;

public class Family {
    private Human mother;
    public Human getMother() {
        return mother;
    }

    private Human father;
    public Human getFather() {
        return father;
    }

    private Human[] children;
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public Human[] getChildren() {
        return children;
    }

    private Pet pet;
    public void setPet(Pet pet) {
        this.pet = pet;
    }
    public Pet getPet() {
        return pet;
    }

    private enum Types {
        MINIMUM("Family{mother=%s, father=%s, children=[]}"),
        MAXIMUM("Family{mother=%s, father=%s, children=%s,pet=%s}");

        String toStringText;
        Types(String toStringText) {this.toStringText = toStringText;}
    }
    private Types type;

    private void constructor(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        type = Types.MINIMUM;
    }

    public Family(Human mother, Human father) {
        constructor(mother, father);
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        constructor(mother, father);
        this.children = Arrays.copyOf(children, children.length);
        for(Human child : children) child.setFamily(this);
        this.pet = pet;
        type = Types.MAXIMUM;
    }

    @Override
    public String toString() {
        return type.equals(Types.MAXIMUM)
                ? String.format(Types.MAXIMUM.toStringText, mother.toString(), father.toString(), Arrays.toString(children), pet.toString())
                : String.format(Types.MINIMUM.toStringText, mother.toString(), father.toString());
    }

    public void addChild(Human child) {
        this.children = Arrays.copyOf(children, children.length + 1);
        children[children.length-1] = child;
        child.setFamily(this);
    }
    public boolean deleteChild(int index) {
        if(index >= children.length || index < 0) return false;
        Human[] newChildren = new Human[children.length-1];
        for(int i  = 0; i < children.length - 1; i++) newChildren[i] = children[i < index ? i : i + 1];
        children[index].setFamily();
        children = newChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        for(int i = 0; i < children.length; i++) {
            if(children[i].equals(child)) return deleteChild(i);
        }
        return false;
    }

    public int countFamily() {return 2 + children.length;}

    {
        System.out.println("Creating object Family");
    }
    static {
        System.out.println("Downloading class Family");
    }

    protected void finalize() {
        System.out.println("Deleting class: " + this);
    }
}
