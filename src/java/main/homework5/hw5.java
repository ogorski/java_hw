package homework5;

public class hw5 {
    public static void main(String[] args) {
        Human father = new Human("Kyle", "Katarn", 32, 145, new String[][] {{"Monday", "Go To Work"}});
        Human mother = new Human("Mary", "Katarn", 30, 143, new String[][] {{"Monday", "Do cleaning"}});
        Human Kate = new Human("Kate", "Katarn", 16, 134, new String[][] {{"Monday", "Go to school"}});
        Human Jane = new Human("Jane", "Katarn", 13, 124, new String[][] {{"Monday", "Go to school"}});
        Pet cat = new Pet("Cat", "Loki", 2, 50, new String[] {"Cat`s are funny"}); 

        Family newFamily = new Family(mother, father, new Human[] {Jane, Kate}, cat);
        System.out.println(newFamily.toString());
        System.out.printf("People in family: %d\n", newFamily.countFamily());
        System.out.println("Before first delete: ");

        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }

        Human Mike = new Human("Mike", "Katarn", 0, 40, new String[0][0]);
        newFamily.addChild(Mike);
        newFamily.deleteChild(1);
        System.out.println("After first delete: ");
        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }
        newFamily.deleteChild(Kate);
        System.out.println("After second delete: ");
        for(Human child : newFamily.getChildren()) {
            System.out.println(child.getName());
        }

        Mike.greetPet();
        father.describePet();
        mother.feedPet(false);
    }
}