package homework5;

public enum Species {
    UNKNOWN,
    DOMESTIC_CAT,
    FISH,
    DOG,
    ROBO_CAT;
}