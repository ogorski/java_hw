package homework6;

public final class Female extends Human{
    public Female() {
        super();
        gender = "Female";
    }
    public Female(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Female";
    }
    public Female(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Female";
    }
    @Override
    public void greetPet() {
        System.out.printf("Hello, fella %s\n", getFamily().getPet().getNickname());
    }
    public void doMakeUp() {
        System.out.println("I need to do that make up");
    }
}
