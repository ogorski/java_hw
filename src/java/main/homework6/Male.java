package homework6;

public final class Male extends Human {
    public Male() {
        super();
        gender = "Male";
    }
    public Male(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
        gender = "Male";
    }
    public Male(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
        gender = "Male";
    }
    @Override
    public void greetPet() {
        System.out.printf("Hello, fella %s\n", getFamily().getPet().getNickname());
    }
    public void makeCar() {
        System.out.println("I need to repair that car");
    }
}
