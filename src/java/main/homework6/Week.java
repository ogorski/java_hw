package homework6;

import java.util.*;

public enum Week {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public String toString() {
        return this.name().toLowerCase(Locale.ROOT);
    }
}
