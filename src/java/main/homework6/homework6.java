package homework6;

import homework6.pet.*;

public class homework6 {
    public static void main(String[] args) {
        Human father = new Human("Julius", "Caesar", 50, 185);
        Male son = new Male("Macrus", "Aurelius", 45, 165);
        Female mother = new Female("Joanna", "V", 28, 125, new String[][] {{"Do make up", "make cake"}});
        Female daughter = new Female("Jane", "Erso", 20, 115);
        DomesticCat cat = new DomesticCat("Julius", 3, 100, args);

        System.out.println(father.toString());
        System.out.println(son.toString());
        System.out.println(mother.toString());
        System.out.println(daughter.toString());
        
        cat.respond();
        cat.foul();
        cat.eat();
        mother.doMakeUp();
        son.makeCar();
    }
}
