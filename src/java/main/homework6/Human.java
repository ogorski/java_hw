package homework6;

import java.util.*;

public class Human {
    private String name;
    private String surname;
    private int iq;
    private int year;
    private String[][] schedule;
    private Family family;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    private boolean isEmpty = false;
    public Human() {
        isEmpty = true;
    }
    protected String gender = "Human";
    private void constructor(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }
    public Human(String name, String surname, int year, int iq) {
        constructor(name, surname, year, iq);
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        constructor(name, surname, year, iq);
        this.schedule = schedule;
    }
    public void greetPet() {
        System.out.printf("Hello, %s\n", family.getPet().getNickname());
    }
    public void describePet() {
        System.out.printf("I have a %s, he`s a %d years old, he is %s\n", family.getPet().getType(), family.getPet().getAge(), family.getPet().getCunningLvl() > 50 ? "almost cunning" : "not even cunning");
    }
    @Override
    public String toString() {
        if(isEmpty) {
            return gender + "";
        }
        String toStringTxt = String.format("%s{name='%s', surname='%s', year=%d, iq=%d", gender, name, surname, year, iq);
        if(schedule == null) {
            return toStringTxt + "}";
        }
        String scheduleToStr = "[";
        for(String[] day : schedule) {
            scheduleToStr += Arrays.toString(day) + (Arrays.equals(schedule[schedule.length - 1], day) ? "]" : ", ");
        }
        return toStringTxt + ", schedule=" + scheduleToStr + "}";
    }
    public boolean feedPet(boolean needToFeed) {
        Random random = new Random();
        if(needToFeed || family.getPet().getCunningLvl() > random.nextInt(100)) {
            System.out.println("Hm... I should feed" + family.getPet().getNickname());
            return true;
        } else {
            System.out.printf("I think, he is not hungry \n", family.getPet().getNickname());
            return false;
        }
    }
    {
        System.out.println("Creating object Human");
    }
    static {
        System.out.println("Downloading class Human");
    }
    protected void finalize() {
        System.out.println("Deleting object: " + this);
    }
}
