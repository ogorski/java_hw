package homework6.pet;

import homework6.Species;

public class Fish extends Pet {
    final Species type = Species.Fish;
    public Fish (String nickname, int age, int cunningLvl, String[] habits) {
        super(nickname, age, cunningLvl, habits);
    }
    @Override
    public void respond() {
        System.out.println("Blem, blem");
    }
    @Override
    public String toString() {
        return getToString(this, "perks={noFur, numberOfLimbs=2}");
    }
    @Override
    public Species getType() {
        return type;
    }
}
