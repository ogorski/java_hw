package homework6.pet;

import homework6.Species;

public class DomesticCat extends Pet implements PetFoul {
    public final Species type = Species.DomesticCat;
    public DomesticCat (String nickname, int age, int cunningLvl, String[] habits) {
        super(nickname, age, cunningLvl, habits);
    }
    @Override
    public void respond() {
        System.out.println("Meow, meow");
    }
    @Override
    public String toString() {
        return getToString(this, "perks={hasFur, numberOfLimbs=4}");
    }
    @Override
    public Species getType() {
        return type;
    }
    @Override
    public void foul() {
        System.out.println(getNickname() + "did crime, and now playin with own tail");
    }
}
