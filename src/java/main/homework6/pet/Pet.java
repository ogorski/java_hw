package homework6.pet;

import java.util.*;
import homework6.Species;

public abstract class Pet {
    private String nickname;
    private int age;
    private int cunningLvl;
    private Species species;
    private String[] habits;

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getCunningLvl() {
        return cunningLvl;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void setCunningLvl(int cunningLvl) {
        this.cunningLvl = cunningLvl;
    }

    public Pet(String nickname, int age, int cunningLvl, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.cunningLvl = cunningLvl;
        this.habits = Arrays.copyOf(habits, habits.length);
    }

    public static void eat() {
        System.out.println("I`m eating!");
    }

    public abstract void respond();

    @Override
    public abstract String toString();
    {
        System.out.println("Creating object Pet");
    }
    static {
        System.out.println("Downloading class Pet");
    }

    protected void finalize() {
        System.out.println("Deleting object: " + this);
    }
    protected static String getToString(Pet link, String special) {
        return String.format("$s{nickname='%', age=%d, cunning level=%d, habits=%s, %s}", link.getType(), link.getNickname(), link.getAge(), link.getCunningLvl(), Arrays.toString(link.getHabits()), special);
    }
    public abstract Species getType();
}
