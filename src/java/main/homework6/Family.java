package homework6;

import homework6.pet.Pet;
import java.util.*;

public class Family {
    private Pet pet;
    private Human mother;
    private Human father;
    private Human[] children;
    private Types type;

    public Pet getPet() {
        return pet;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }
    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human[] getChildren() {
        return children;
    }
    public void setChildren(Human[] children) {
        this.children = children;
    }
    private enum Types {
        MINIMUM("Family{father=%s, mother=%s, children=[]}"),
        MAXIMUM("Family{father=%s, mother=%s, children=%s, pet=%s}");
        String toStringTxt;
        Types (String toStringTxt) {
            this.toStringTxt = toStringTxt;
        }
    }
    private void constructor(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        this.father.setFamily(this);
        this.mother.setFamily(this);
        type = Types.MINIMUM;
    }
    public Family(Human father, Human mother) {
        constructor(father, mother);
    }
    public Family(Human father, Human mother, Human[] children, Pet pet) {
        constructor(father, mother);
        this.children = Arrays.copyOf(children, children.length);
        for (Human child : children) {
            child.setFamily(this);
        }
        this.pet = pet;
        type = Types.MAXIMUM;
    }
    @Override
    public String toString() {
        return type.equals(Types.MAXIMUM)
                ? String.format(Types.MAXIMUM.toStringTxt, father.toString(), mother.toString(), Arrays.toString(children), pet.toString())
                : String.format(Types.MINIMUM.toStringTxt, father.toString(), mother.toString());
    }
    public void addChild(Human child) {
        this.children = Arrays.copyOf(children, children.length + 1);
        children[children.length - 1] = child;
        child.setFamily(this);
    }
    public boolean deleteChild(int idx) {
        if (idx >= children.length || idx < 0) {
            return false;
        }
        Human[] newChild = new Human[children.length - 1];
        for (int i = 0; i < children.length - 1; i++) {
            newChild[i] = children[i < idx ? i : i + 1];
        }
        children[idx].setFamily(this);
        children = newChild;
        return true;
    }
    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                return deleteChild(i);
            }
        }
        return false;
    }
    public int countFamily() {
        return 2 + children.length;
    }
    {
        System.out.println("Creating object Family");
    }
    static {
        System.out.println("Downloading class Family");
    }
    protected void finalize() {
        System.out.println("Deleting object: " + this);
    }
}