package homework6;

public enum Species {
    DomesticCat,
    Dog,
    RoboCat,
    Fish,
    UNKNOWN;
}
