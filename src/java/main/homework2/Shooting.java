package homework2;

import java.util.*;

public class Shooting {
    public static void print(String s) {
        System.out.print(s);
    }
    static int random_Integer(int from, int to) {
        int count = (int) (Math.random()*(to - from + 1) + from);
        return count;
    }
    static String[][] matrix(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                print("\t" + array[i][j] + "\t" + "|");
            }
            System.out.println();
        };
        return array;
    }
    static String[][] matrix(String n, String[][] array, int shootRow, int shootCol) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[shootRow][shootCol] = n;
                print("\t" + array[i][j] + "\t" + "|");
            }
            System.out.println();
        }
        return array;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int targetRw = random_Integer(1, 5);
        int targetCl = random_Integer(1, 5);
        print ("All is set! Let`s rumble! \n");
        String[][] gameField = {
            {"0","1","2","3","4","5"},
            {"1","-","-","-","-","-"},
            {"2","-","-","-","-","-"},
            {"3","-","-","-","-","-"},
            {"4","-","-","-","-","-"},
            {"5","-","-","-","-","-"},
        };
        String[][] field = matrix(gameField);
        for (; ;) {
            print("\n Input target col: ");
            int shootCol = in.nextInt();
            print("\n Input target row: ");
            int shootRow = in.nextInt();
            if (shootCol > gameField.length - 1) {
                print("\n Try again! Wrong col");
            } if (shootRow > gameField.length - 1) {
                print("\n Try again! Wrong row");
            } if (shootRow <= gameField.length - 1 && shootCol <= gameField.length - 1) {
                print("\n");
                String[][] game = matrix("*", gameField, shootRow, shootCol);
            } if (targetRw == shootRow && targetCl == shootCol) {
                print("You win! \n");
                String[][] l = matrix("X", gameField, shootRow, shootCol);
                break;
            }
        }
    }
}