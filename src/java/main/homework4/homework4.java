package homework4;

public class homework4 {
  public static void main(String[] args) {
    Human father = new Human("Kyle", "katarn", 34, 145, new String[][] {{"Monday", "Go to work"}, {"Sunday", "Relax"}});
    Human mother = new Human("Jyn", "katarn", 32, 145, new String[][] {{"All days except output", "go to work"}});
    Human Kate = new Human("Kate", "katarn", 18, 130, new String[][] {{"Monday", "Go to university"}, {"All days except saturday and sunday", "go to university"}});
    Human Jane = new Human("Jane", "katarn", 15, 120, new String[][] {{"Monday", "Go to school"}, {"All days except saturday and sunday", "go to school"}});
    Pet cat = new Pet("Cat", "Loki", 2, 50, new String[] {"Cat`s are funny"});

    Family newFamily = new Family(father, mother, new Human[] {Kate, Jane}, cat);
    System.out.println(newFamily.toString());
    System.out.printf("Count people in family: %d/n", newFamily.countFamily());
    System.out.println("Before first deletion");

    for(Human child : newFamily.getChild()) {
      System.out.println(child.getName());
    }

    Human Vera = new Human("Vera", "katarn", 0, 5, new String[0][0]);
    newFamily.addChild(Vera);
    newFamily.deleteChild(1);
    System.out.println("After first delete: ");
    for(Human child : newFamily.getChild()) {
      System.out.println(child.getName());
    }
    newFamily.deleteChild(Kate);
    System.out.println("After second delete: ");
    for(Human child : newFamily.getChild()) {
      System.out.println(child.getName());
    }
    Vera.greetPet();
    father.describePet();
    mother.feedPet(false);
  }
}
