package homework4;

import java.util.*;

public class Pet {
    private String nickname;
    private String species;
    private String[] habits;

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getNickname() {
        return nickname;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public String getSpecies() {
        return species;
    }

    private int age;
    private int cunningLevel;

    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return age;
    }
    public void setCunningLevel(int cunningLevel) {
        this.cunningLevel = cunningLevel;
    }
    public int getCunningLevel() {
        return cunningLevel;
    }

    private enum ConstructorTypes {
        SMALL("%s{nickname='%s'}"),
        MAXIMUM("%s{nickname='%s', age=%d, cunningLevel=%d, habits=%s}"),
        EMPTY("pet{}");
        String toStringText;
        ConstructorTypes(String toStringText) {
            this.toStringText = toStringText;
        }
    }
    private final ConstructorTypes currentType;

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.currentType = ConstructorTypes.SMALL;
    }

    public Pet(String species, String nickname, int age, int cunningLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.cunningLevel = cunningLevel;
        this.habits = Arrays.copyOf(habits, habits.length);

        this.currentType = ConstructorTypes.MAXIMUM;
    }

    public Pet() {
        this.currentType = ConstructorTypes.EMPTY;
    }

    public static void eat() {
        System.out.println("I`m eating!");
    }
    public void respond() {
        System.out.printf("\nHello, human. Я - %s. I miss you!", nickname);
    }
    public static void foul() {
        System.out.println("Need to get clean...");
    }
    @Override
    public String toString() {
        return String.format(currentType.toStringText, species, nickname, age, cunningLevel, Arrays.toString(habits));
    }

    {
        System.out.println("Creating class Pet");
    }
    static {
        System.out.println("Downloading class Pet");
    }
}
