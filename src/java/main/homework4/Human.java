package homework4;

import java.util.*;

public class Human {
    private String name;
    private String surname;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return surname;
    }

    private int year;
    private int iq;
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return year;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public int getIq() {
        return iq;
    }

    private String[][] schedule;
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    public String[][] getSchedule() {
        return schedule;
    }

    private boolean isEmpty = false;

    private Family family;
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setFamily() {
        this.family = null;
    }

    public Human() {
        isEmpty = true;
    }
    private void constructor(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human(String name, String surname, int year, int iq) {
        constructor(name, surname, year, iq);
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        constructor(name, surname, year, iq);
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.printf("Hello, %s\n", family.getPet().getNickname());
    }
    public void describePet() {
        System.out.printf("I have %s, he`s %d years old, he is %s\n", family.getPet().getSpecies(), family.getPet().getAge(), family.getPet().getCunningLevel() > 50 ? "very cunning" : "not cunning");
    }
    @Override
    public String toString() {
        if(isEmpty) return "Human{}";
        String scheduleToString = "[";
        for(String[] day : schedule) {
            scheduleToString += Arrays.toString(day) + (Arrays.equals(schedule[schedule.length-1], day) ? "]" : ", ");
        }
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, schedule=%s}", name, surname, year, iq, scheduleToString);
    }

    public boolean feedPet(boolean isItNeedToFeed) {
        Random random = new Random();
        if(isItNeedToFeed || family.getPet().getCunningLevel() > random.nextInt(101)) {
            System.out.println("Hmm.. I should feet " + family.getPet().getNickname());
            return true;
        } else {
            System.out.printf("I think, %s not hungry.\n", family.getPet().getNickname());
            return false;
        }
    }

    {
        System.out.println("Creating class Human");
    }
    static {
        System.out.println("Downloading class Human");
    }
}
